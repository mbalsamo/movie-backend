const express = require('express');
const mongodb = require('mongodb');
const bodyParser = require('body-parser');
const ObjectID = require('mongodb').ObjectID;

const movies = require('./apis/movies');
const users = require('./apis/users') //go get users.js
const reviews = require('./apis/reviews')

const app = express();
const PORT = 5050;

var cors = require('cors')
app.use(cors())

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*")
  res.header("Access-Control-Allow-Headers", "Content-Type")
  res.setHeader("Access-Control-Allow-Methods", "GET, POST, OPTIONS, PUT, PATCH, DELETE")

  next();
})


app.use(bodyParser.urlencoded({extended: false})); //allow user to send data within URL
app.use(bodyParser.json()); //allow user to send JSON data

app.use('/movies', movies);
app.use('/users', users)
app.use('/reviews', reviews)

app.listen(PORT);
console.log("Listening on port " + PORT);
